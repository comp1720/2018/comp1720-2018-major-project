# COMP1720 major project template

This is the big one! Make sure you read the [assignment page](https://cs.anu.edu.au/courses/comp1720/deliverables/05-major-project/) carefully, and if
you have questions then ask for help **early** on [Discourse](https://discourse.cecs.anu.edu.au/c/comp1720/major-project).

<https://cs.anu.edu.au/courses/comp1720/deliverables/05-major-project/>

## Repo structure

- `master` is the very base branch - contains the p5 template and nothing else
- `lab-base` is the base lab/assignment repo branch, which includes the stuff
  from `master` plus the CI file for deploying to the test URL
- `lab-{1..9}` contains the starter code for each lab exercise
- `assignment-{1..3}` contains the starter code for each assignment
- `major-project` contains the starter code for the major project

The assignment and major project CI files (`.gitlab-ci.yml`) also include some
deliverable-specific CI checks (e.g. ass1 checks that the `nametag.png` has been
added)

- the `lecture-livecode` branch is used throughout the whole course, and is
  tagged each week with the "results" of that week's livecoding
